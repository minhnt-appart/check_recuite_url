<?php

namespace App\Commands;

use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class CheckUrlCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'check:url';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'check available url';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \DB::table('companies')
            ->select([
                'id',
                'recruit_url',
                'url_check',
            ])
            ->where(function ($query) {
                $query->orWhereNull('url_check')->orWhere('url_check', 1);
            })
            ->chunkById(10, function ($companies) {
                foreach ($companies as $company) {
                    try {
                        $url = $company->recruit_url ?? null;
                        $url_check = 0;
                        if (!empty($url)) {
                            // option 1

                            $response = \Illuminate\Support\Facades\Http::retry(1, 30)->get($url);
                            $status = $response->status();
                            // --------
                            // other methods
                            // $response->body();
                            // $response->json();
                            // $response->status();
                            // $response->ok();
                            // $response->successful();
                            // $response->failed();
                            // $response->serverError();
                            // $response->clientError();
                            // $response->headers();
                            // ---------

                            // option 2

                            // $status = (int) $this->getUrlCode($url);

                            if (\in_array($status, [400, 0, 404, 403])) {
                                $this->error("$status: $url");
                                $url_check = 0;
                            } else {
                                // $status = 200 => url is available
                                $this->info("$status: $url");
                                $url_check = 1;
                            }
                            // In the case of status = 403 (redirect url) you can continue to code
                        }
                    } catch (\Throwable $th) {
                        $url_check = 0;
                    }
                    $update = \DB::table('companies')
                        ->where('id', $company->id)
                        ->update([
                            'url_check' => $url_check,
                        ]);
                }
            }, 'id')
        ;
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }

    private function getUrlCode(string $url)
    {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HEADER, true); // we want headers
        curl_setopt($ch, CURLOPT_NOBODY, true); // we don't need body
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        $output = curl_exec($ch);
        $httpcode = (int) curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        return $httpcode;
    }
}
