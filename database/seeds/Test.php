<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class Test extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = \Faker\Factory::create();

        for ($i = 0; $i < 1000; $i++) {
            \DB::table('companies')->insert([
                'recruit_url' => $faker->url,
                'url' => $faker->url,
                'url_check' => null,
            ]);
        }
    }
}
